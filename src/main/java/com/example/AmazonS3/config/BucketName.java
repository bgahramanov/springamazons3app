package com.example.AmazonS3.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BucketName {
    TODO_IMAGE("spring-boot-amazon-s3");
    private final String bucketName;
}
